TOPDIR = .

LOAD_PATH = $(TOPDIR)/scheme
TEST_PATH = $(TOPDIR)/tests
GUILE_BINARY ?= guile
GUILE_CALL = $(GUILE_BINARY) -L $(LOAD_PATH) -C $(LOAD_PATH) --no-auto-compile
GUILD_BINARY ?= guild

RUNTESTS = SCHEME_INTERPRETER="$(GUILE_BINARY)" run-tests
RUNTESTS += -strip-roots -dispatch-root "$(TEST_PATH)"
INSTALL = $(GUILE_CALL) $(TOPDIR)/tools/install

CFLAGS = -Wunsupported-warning -Wunused-variable -Wunused-toplevel
CFLAGS += -Wunbound-variable -Warity-mismatch -Wduplicate-case-datum
CFLAGS += -Wbad-case-datum -Wformat -L$(LOAD_PATH)

COMPILE = $(GUILD_BINARY) compile $(CFLAGS)

MODULES  = $(TOPDIR)/scheme/interpreter/closure.scm
MODULES += $(TOPDIR)/scheme/interpreter/environment.scm
MODULES += $(TOPDIR)/scheme/interpreter/eval-apply.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/boolean.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/combinators.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/convert.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/natural-numbers.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/pair.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/sugar.scm
MODULES += $(TOPDIR)/scheme/interpreter/lambda/with-primitives.scm
MODULES += $(TOPDIR)/scheme/interpreter/primitive.scm
MODULES += $(TOPDIR)/scheme/interpreter/repl.scm
OBJECTS = ${MODULES:.scm=.go}

.SUFFIXES: .scm .go

all:
	@echo
	@echo " Available targets:"
	@echo
	@echo "           all: This help text"
	@echo "       compile: Byte-compile the scheme library"
	@echo "         clean: Remove byte-compiled scheme code"
	@echo "       install: Install the project to the host system"
	@echo "          test: Run test suite"
	@echo "  test-verbose: Run test suite (with verbose test harness)"
	@echo "    test-debug: Run test suite (With all debugging enabled)"
	@echo

.scm.go:
	$(COMPILE) -o $@ $<

compile: $(OBJECTS)

clean:
	find . -name "*.go" -exec rm -f '{}' +
	find . -name "*~" -exec rm -f '{}' +
	find . -name "*.failure" -exec rm -f '{}' +

doc:
	@(cd doc && $(MAKE) all;)

install:
	$(INSTALL)

repl:
	@rlwrap ./yamcscm

test:
	$(RUNTESTS)

test-verbose:
	$(RUNTESTS) -verbose

test-debug:
	$(RUNTESTS) -verbose -dispatch-verbose -debug

.PHONY: all compile clean clean doc install repl test test-debug test-verbose
