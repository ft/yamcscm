;; -*- scheme -*-

(define-module (interpreter lambda combinators)
  #:use-module (srfi srfi-9 gnu)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda)
  #:export (combinators
            combinator-table
            combinator->closure
            combinator->expression))

(define-immutable-record-type <combinator>
  (make-combinator name short-hand expression closure)
  combinator?
  (name combinator-name)
  (short-hand combinator-short-hand)
  (expression combinator-expression)
  (closure combinator-closure))

;; Here are a lot of combinators. They got almost official bird names by now,
;; based on the 1985 book “To Mock a Mockingbird”¹. While writing up a number
;; of these, that come to mind, I came across a site that maintains a compre-
;; hensive list of combinators from the book². This module was extended using
;; that list. This only defines the combinators that have α-normal form.
;;
;; ¹ https://en.wikipedia.org/wiki/To_Mock_a_Mockingbird
;; ² http://www.angelfire.com/tx4/cus/combinator/birds.html

(define-syntax-rule (generate-combinator-table (n s e) ...)
  (list (make-combinator 'n 's 'e (λ-eval 'e)) ...))

(define combinator-table
  (generate-combinator-table
   (bluebird                     B    (λa → λb → λc → a (b c)))
   (blackbird                    B1   (λa → λb → λc → λd → a (b c d)))
   (bunting                      B2   (λa → λb → λc → λd → λe → a (b c d e)))
   (becard                       B3   (λa → λb → λc → λd → a (b (c d))))
   (cardinal                     C    (λa → λb → λc → a c b))
   (dove                         D    (λa → λb → λc → λd → a b (c d)))
   (dickcissel                   D1   (λa → λb → λc → λd → λe → a b c (d e)))
   (dovekies                     D2   (λa → λb → λc → λd → λe → a (b c (d e))))
   (eagle                        E    (λa → λb → λc → λd → λe → a b (c d e)))
   (bald-eagle                   Ê    (λa → λb → λc → λd → λe → λf → λg → a (b c d (e f g))))
   (finch                        F    (λa → λb → λc → c b a))
   (goldfinch                    G    (λa → λb → λc → λd → a d (b c)))
   (hummingbird                  H    (λa → λb → λc → a b c b))
   (identity                     I    (λa → a))
   (jay                          J    (λa → λb → λc → λd → a b (a d c)))
   (kestrel                      K    (λa → λb → a))
   (kite                         KI   (λa → λb → b))
   (lark                         L    (λa → λb → a (b b)))
   (mockingbird                  M    (λa → a a))
   (double-mockingbird           M2   (λa → λb → a b (a b)))
   (owl                          O    (λa → λb → b (a b)))
   (queer-bird                   Q    (λa → λb → λc → b (a c)))
   (quixotic-bird                Q1   (λa → λb → λc → a (c b)))
   (quizzical-bird               Q2   (λa → λb → λc → b (c a)))
   (quirky-bird                  Q3   (λa → λb → λc → c (a b)))
   (quacky-bird                  Q4   (λa → λb → λc → c (b a)))
   (robin                        R    (λa → λb → λc → b c a))
   (starling                     S    (λa → λb → λc → a c (b c)))
   (thrush                       Th   (λa → λb → b a))
   (turing                       U    (λa → λb → b (a a b)))
   (vireo                        V    (λa → λb → λc → c a b))
   (warbler                      W    (λa → λb → a b b))
   (converse-warbler             W1   (λa → λb → b a a))
   (why-bird                     Y    (λf → (λx → f (x x)) (λx → f (x x))))
   (identity-bird-once-removed   I*   (λa → λb → a b))
   (warbler-once-removed         W*   (λa → λb → λc → a b c c))
   (cardinal-once-removed        C*   (λa → λb → λc → λd → a b d c))
   (robin-once-removed           R*   (λa → λb → λc → λd → a c d b))
   (finch-once-removed           F*   (λa → λb → λc → λd → a d c b))
   (vireo-once-removed           V*   (λa → λb → λc → λd → a c b d))
   (identity-bird-twice-removed  I**  (λa → λb → λc → a b c))
   (warbler-twice-removed        W**  (λa → λb → λc → λd → a b c d d))
   (cardinal-twice-removed       C**  (λa → λb → λc → λd → λe → a b c e d))
   (robin-twice-removed          R**  (λa → λb → λc → λd → λe → a b d e c))
   (finch-twice-removed          F**  (λa → λb → λc → λd → λe → a b e d c))
   (vireo-twice-removed          V**  (λa → λb → λc → λd → λe → a b e c d))
   (konstant-mocker              KM   (λa → λb → b b))
   (crossed-konstant-mocker      CKM  (λa → λb → a a))))

(define combinators
  (make-environment (append (map (lambda (c)
                                   (cons (combinator-name c)
                                         (combinator-closure c)))
                                 combinator-table)
                            (map (lambda (c)
                                   (cons (combinator-short-hand c)
                                         (combinator-closure c)))
                                 combinator-table))))

(define (fetch-combinator name accessor)
  (accessor (let loop ((rest combinator-table))
              (if (null? rest)
                  (throw 'no-such-combinator name)
                  (let ((this (car rest)))
                    (cond ((eq? name (combinator-name this)) this)
                          ((eq? name (combinator-short-hand this)) this)
                          (else (loop (cdr rest)))))))))

(define (combinator->expression name)
  (fetch-combinator name combinator-expression))

(define (combinator->closure name)
  (fetch-combinator name combinator-closure))
