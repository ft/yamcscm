;; -*- scheme -*-

(define-module (interpreter closure)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-closure
            closure?
            closure-formals
            closure-body
            closure-environment))

(define-immutable-record-type <closure>
  (make-closure formals body environment)
  closure?
  (formals closure-formals)
  (body closure-body)
  (environment closure-environment))
