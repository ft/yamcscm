;; -*- scheme -*-

(define-module (interpreter environment)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-environment
            environment?
            make-empty-environment
            empty-environment?
            environment-table
            extend
            extend*
            lookup
            use))

(define-immutable-record-type <environment>
  (make-environment table)
  environment?
  (table environment-table extend-environment-table))

(define (make-empty-environment)
  (make-environment '()))

(define (empty-environment? env)
  (zero? (length (environment-table env))))

(define (lookup env sym)
  (let loop ((rest (environment-table env)))
    (if (null? rest)
        (error "Unknown variable:" sym env)
        (let ((this (car rest)))
          (if (eq? (car this) sym)
              (cdr this)
              (loop (cdr rest)))))))

(define (extend e s v)
  (extend-environment-table e (cons (cons s v) (environment-table e))))

(define (extend* env formals operands)
  (if (list? formals)
      (fold (lambda (k v a) (extend a k v))
            env formals operands)
      (extend env formals operands)))

(define (use . lst)
  (make-environment (apply append (map environment-table lst))))
