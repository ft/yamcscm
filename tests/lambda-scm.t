;; -*- scheme -*-

(use-modules (ice-9 match)
             (test tap)
             (test setup)
             (interpreter closure)
             (interpreter environment)
             (interpreter lambda)
             (interpreter lambda boolean)
             (interpreter lambda convert)
             (interpreter lambda natural-numbers)
             (interpreter lambda pair))

(init-test-tap!)

(define λ-identity (λ-eval '(λx → x)))
(define λ-fish (λ-eval '(λfish → fish)))
(define λ-thing (λ-eval '(λthing → thing)))

(define y-combinator '(λf → (λg → g g)
                            (λh → f (λa → h h a))))

(define te (make-environment `((identity . ,λ-identity)
                               (fish     . ,λ-fish)
                               (thing    . ,λ-thing))))

(define (scheme-factorial n)
  (if (zero? n)
      1
      (* n (scheme-factorial (1- n)))))

(define head-factorial '(λr → λx →
                              (if (zero? x)
                                  (λ_ → (successor zero))
                                  (λ_ → (* x (r (predecessor x)))))))

(define ackermann '(λn → n (λf → λm → m f (f (successor zero))) successor))
(define ackermann-table '((0 0     1) (0 1     2) (0 2  3) (0 3  4) (0 4   5)
                          (1 0     2) (1 1     3) (1 2  4) (1 3  5) (1 4   6)
                          (2 0     3) (2 1     5) (2 2  7) (2 3  9) (2 4  11)
                          (3 0     5) (3 1    13) (3 2 29) (3 3 61) (3 4 125)
                          (4 0    13)))

(with-test-bundle (interpreter lambda)
  (plan (+ 46 (length ackermann-table)))

  (define-test "Lambda expression evaluates to closure"
    (pass-if-true (closure? (λ-eval '(λx → x)))))
  (define-test "Application works (identity identity) => identity"
    (pass-if-equal? (λ-eval '(identity identity) te)
                    λ-identity))

  (define-test "true returns its first argument"
    (pass-if-equal? (λ-eval '(true thing fish) (use te boolean))
                    λ-thing))
  (define-test "false returns its second argument"
    (pass-if-equal? (λ-eval '(false thing fish) (use te boolean))
                    λ-fish))

  (define-test "(not true) => false"
    (pass-if-equal? (λ-eval '(not true) boolean)
                    (lookup boolean 'false)))
  (define-test "(not false) => true"
    (pass-if-equal? (λ-eval '(not false) boolean)
                    (lookup boolean 'true)))

  (define-test "(or false false) => false"
    (pass-if-false (λ-bool->scheme (λ-eval '(or false false) boolean))))
  (define-test "(or false true) => true"
    (pass-if-true (λ-bool->scheme (λ-eval '(or false true) boolean))))
  (define-test "(or true false) => true"
    (pass-if-true (λ-bool->scheme (λ-eval '(or true false) boolean))))
  (define-test "(or true true) => true"
    (pass-if-true (λ-bool->scheme (λ-eval '(or true true) boolean))))

  (define-test "(and false false) => false"
    (pass-if-false (λ-bool->scheme (λ-eval '(and false false) boolean))))
  (define-test "(and false true) => false"
    (pass-if-false (λ-bool->scheme (λ-eval '(and false true) boolean))))
  (define-test "(and true false) => false"
    (pass-if-false (λ-bool->scheme (λ-eval '(and true false) boolean))))
  (define-test "(and true true) => true"
    (pass-if-true (λ-bool->scheme (λ-eval '(and true true) boolean))))

  (define-test "if true picks consequence"
    (pass-if-equal? (λ-eval '(if true (λ_ → thing) (λ_ → fish))
                            (use te boolean))
                    λ-thing))
  (define-test "if false picks alternative"
    (pass-if-equal? (λ-eval '(if false (λ_ → thing) (λ_ → fish))
                            (use te boolean))
                    λ-fish))

  (let ((foo (λ-eval '(cons true false) (use boolean pair))))
    (define-test "First item of foo pair is true"
      (pass-if-true (λ-bool->scheme (λ-eval `(first ,foo) pair))))
    (define-test "Second item of foo pair is false"
      (pass-if-false (λ-bool->scheme (λ-eval `(second ,foo) pair)))))

  (let ((one '(successor zero))
        (two '(successor (successor zero)))
        (three '(successor (successor (successor zero)))))
    (define-test "zero encodes 0"
      (pass-if-= (λ-natural-number->scheme (λ-eval 'zero natural-numbers))
                 0))
    (define-test "(successor zero) encodes 1"
      (pass-if-= (λ-natural-number->scheme (λ-eval one natural-numbers))
                 1))
    (define-test "(successor (successor zero)) encodes 2"
      (pass-if-= (λ-natural-number->scheme (λ-eval two natural-numbers))
                 2))
    (define-test "(successor (successor (successor zero))) encodes 3"
      (pass-if-= (λ-natural-number->scheme (λ-eval three natural-numbers))
                 3))
    (define-test "(+ three two) evaluates to a church numeral encoding 5"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(+ ,three ,two)
                                                   natural-numbers))
                 5))
    (define-test "(* three two) evaluates to a church numeral encoding 6"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(* ,three ,two)
                                                   natural-numbers))
                 6))
    (define-test "(^ three two) evaluates to a church numeral encoding 9"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(^ ,three ,two)
                                                   natural-numbers))
                 9))
    (define-test "(^ two three) evaluates to a church numeral encoding 8"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(^ ,two ,three)
                                                   natural-numbers))
                 8))
    (define-test "zero can be identified as zero"
      (pass-if-true (λ-bool->scheme (λ-eval '(zero? zero)
                                            natural-numbers))))
    (define-test "one isn't zero as far as zero? is concerned"
      (pass-if-false (λ-bool->scheme (λ-eval `(zero? ,one)
                                             natural-numbers))))
    (define-test "two isn't zero as far as zero? is concerned"
      (pass-if-false (λ-bool->scheme (λ-eval `(zero? ,two)
                                             natural-numbers))))
    (define-test "predecessor of one is zero"
      (pass-if-true (λ-bool->scheme (λ-eval `(zero? (predecessor ,one))
                                            natural-numbers))))
    (define-test "predecessor of two is 1"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(predecessor ,two)
                                                   natural-numbers))
                 1))
    (define-test "predecessor of predecessor of two is zero"
      (pass-if-true (λ-bool->scheme
                     (λ-eval `(zero? (predecessor (predecessor ,two)))
                             natural-numbers))))
    (define-test "predecessor of three is 2"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(predecessor ,three)
                                                   natural-numbers))
                 2))
    (define-test "predecessor of predecessor of three is 1"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(predecessor
                                                     (predecessor ,three))
                                                   natural-numbers))
                 1))
    (define-test "predecessor predecessor of predecessor of three is zero"
      (pass-if-true (λ-bool->scheme
                     (λ-eval `(zero? (predecessor
                                      (predecessor
                                       (predecessor ,three))))
                             natural-numbers))))
    (define-test "three minus one is two"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(- ,three ,one)
                                                   natural-numbers))
                 2))
    (define-test "three minus two is one"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(- ,three ,two)
                                                   natural-numbers))
                 1))
    (define-test "three minus three is zero"
      (pass-if-= (λ-natural-number->scheme (λ-eval `(- ,three ,three)
                                                   natural-numbers))
                 0))
    (define-test "three minus three is zero according to zero?"
      (pass-if-true (λ-bool->scheme (λ-eval `(zero? (- ,three ,three))
                                            natural-numbers))))
    (define-test "three <= three"
      (pass-if-true (λ-bool->scheme (λ-eval `(<= ,three ,three)
                                            natural-numbers))))
    (define-test "two <= three"
      (pass-if-true (λ-bool->scheme (λ-eval `(<= ,two ,three)
                                            natural-numbers))))
    (define-test "three <= two does not hold"
      (pass-if-false (λ-bool->scheme (λ-eval `(<= ,three ,two)
                                             natural-numbers))))
    (define-test "two = two"
      (pass-if-true (λ-bool->scheme (λ-eval `(= ,two ,two)
                                            natural-numbers))))
    (define-test "two = three does not hold"
      (pass-if-false (λ-bool->scheme (λ-eval `(= ,two ,three)
                                             natural-numbers))))
    (define-test "one = two does not hold"
      (pass-if-false (λ-bool->scheme (λ-eval `(= ,one ,two)
                                             natural-numbers)))))

  (let* ((s-start 10)
         (λ-start (scheme->λ-natural-number s-start))
         (expected (scheme-factorial s-start)))
    (define-test (format #f "(factorial ~a) -> ~a" s-start expected)
      (pass-if-= (λ-natural-number->scheme
                  (λ-eval `((,y-combinator ,head-factorial) ,λ-start)
                          (use boolean natural-numbers)))
                 expected)))

  (for-each (lambda (v)
              (match v
                ((n m r)
                 (define-test (format #f "(ackermann ~a ~a) => ~a" n m r)
                   (pass-if-= r (λ-natural-number->scheme
                                 (λ-eval
                                  `(,ackermann ,(scheme->λ-natural-number n)
                                               ,(scheme->λ-natural-number m))
                                  (use natural-numbers))))))))
            ackermann-table))
