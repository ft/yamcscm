;; -*- scheme -*-

(define-module (interpreter lambda pair)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda)
  #:use-module (interpreter lambda combinators)
  #:export (pair))

(define K (lookup combinators 'K))
(define KI (lookup combinators 'KI))

(define λ-cons   (lookup combinators 'V))
(define λ-first  (λ-eval `(λp → p ,K)))
(define λ-second (λ-eval `(λp → p ,KI)))

(define pair (make-environment `((cons   . ,λ-cons)
                                 (first  . ,λ-first)
                                 (second . ,λ-second))))
