;; -*- scheme -*-

(use-modules (test tap)
             (test setup)
             (interpreter closure)
             (interpreter environment)
             (interpreter eval-apply)
             (interpreter primitive))

(init-test-tap!)

(define y-combinator '(lambda (f)
                        ((lambda (g) (g g))
                         (lambda (h)
                           (f (lambda a (apply (h h) a)))))))

(define tail-factorial '(lambda (r)
                          (lambda (x acc)
                            (if (< x 2)
                                acc
                                (r (- x 1) (* x acc))))))

(define head-factorial '(lambda (r)
                          (lambda (x)
                            (if (< x 2)
                                1
                                (* x (r (- x 1)))))))

(with-test-bundle (interpreter fun eval)
  (plan 9)

  (define-test "Literal numbers evaluate to themselves"
    (let ((number 23))
      (pass-if-= (xeval number)
                 number)))

  (define-test "Symbols of primitives resolve to their primitive type"
    (pass-if-true (primitive? (xeval '+))))

  (define-test "Symbols resolve to their environment values"
    (let ((number 23))
      (pass-if-= (xeval 'foo (make-environment `((foo . ,number))))
                 number)))

  (define-test "Lambda expression evaluates to closure"
    (pass-if-true (closure? (xeval '(lambda (x) x)))))

  (define-test "Identity function returns argument verbatim"
    (let ((number 23))
      (pass-if-= (xeval `((lambda (x) x) ,number))
                 number)))

  (define-test "Conditional to consequence works"
    (pass-if-= (xeval '(if (zero? 0) 23 42))
               23))

  (define-test "Conditional to alternative works"
    (pass-if-= (xeval '(if (zero? 10) 23 42))
               42))

  (define-test "Head-recursive factorial(12) using Y combinator"
    (pass-if-= (xeval `((,y-combinator ,head-factorial) 12))
               479001600))

  (define-test "Tail-recursive factorial(12) using Y combinator"
    (pass-if-= (xeval `((lambda (x) ((,y-combinator ,tail-factorial)
                                     x 1))
                        12))
               479001600)))
