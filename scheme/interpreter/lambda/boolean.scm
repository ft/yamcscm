;; -*- scheme -*-

(define-module (interpreter lambda boolean)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda)
  #:use-module (interpreter lambda combinators)
  #:export (boolean))

(define λ-true  (lookup combinators 'K))
(define λ-false (lookup combinators 'KI))

(define basic
  (make-environment `((true  . ,λ-true)
                      (false . ,λ-false))))

(define λ-and (λ-eval '(λp → λq → p q false) basic))
(define λ-or  (λ-eval '(λp → λq → p true q) basic))
(define λ-not (λ-eval '(λp → p false true) basic))
(define λ-if  (λ-eval `(λp → λc → λa → ((p c a) ,λ-true))))

(define boolean
  (use (make-environment `((and . ,λ-and)
                           (or  . ,λ-or)
                           (not . ,λ-not)
                           (if  . ,λ-if)))
       basic))
