;; -*- scheme -*-

(define-module (interpreter lambda convert)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda boolean)
  #:use-module (interpreter lambda with-primitives)
  #:export (λ-bool->scheme
            λ-natural-number->scheme
            scheme->λ-natural-number))

(define (λ-bool->scheme bool)
  (cond ((eq? bool (lookup boolean 'true)) #t)
        ((eq? bool (lookup boolean 'false)) #f)
        (else (error "Not a boolean:" bool))))

(define (λ-natural-number->scheme n)
  (λp-eval `((,n 1+) 0)))

(define (scheme->λ-natural-number n)
  (let loop ((n n) (acc 'zero))
    (if (zero? n)
        acc
        (loop (1- n) (list 'successor acc)))))
