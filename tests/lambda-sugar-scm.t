;; -*- scheme -*-

(use-modules (test tap)
             (test setup)
             (interpreter lambda sugar))

(init-test-tap!)

(define (sugar-test sugared desugared)
  (define-test (format #f "~a transforms to ~a" sugared desugared)
    (pass-if-equal? (desugar sugared) desugared)))

(with-test-bundle (interpreter lambda sugar)
  (plan 7)
  (for-each (lambda (expr)
              (sugar-test expr expr))
            '(i-am-a-trivial-expression (λ (x) x)))
  (sugar-test '(λx → x)
              '(λ (x) x))
  (sugar-test '(λa → a b)
              '(λ (a) (a b)))
  (sugar-test '(λa → a b c)
              '(λ (a) ((a b) c)))
  (sugar-test '(λa → a b c d)
              '(λ (a) (((a b) c) d)))
  (sugar-test '(λa → λb → a b)
              '(λ (a) (λ (b) (a b)))))
