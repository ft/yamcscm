;; -*- scheme -*-

(define-module (interpreter lambda sugar)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (desugar))

(define (sugared-lambda? e)
  (and (symbol? e)
       (eqv? #\λ (string-ref (symbol->string e) 0))))

(define (desugared-argument e)
  (string->symbol (substring (symbol->string e) 1)))

(define (not-lambda? e)
  (not (eq? 'λ e)))

(define (desugar e)
  (match e
    ;; Turn (λx → body) into (λ (x) body)
    (((? sugared-lambda? l) '→ b)
     `(λ (,(desugared-argument l)) ,b))
    (((? sugared-lambda? l) '→ b c ..1)
     `(λ (,(desugared-argument l)) ,(desugar (cons b c))))
    ;; Turn (a b c d) into (((a b) c) d)
    (((? not-lambda? op) a0 a1 . rest)
     (reduce (lambda (a b) (list b a))
             '()
             (cons* op a0 a1 rest)))
    (_ e)))
