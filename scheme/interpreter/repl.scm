;; -*- scheme -*-

(define-module (interpreter repl)
  #:use-module (interpreter eval-apply)
  #:export (repl))

(define (repl)
  (display "> ")
  (let ((next (read)))
    (when (eof-object? next)
      (quit 0))
    (display (xeval next)))
  (newline)
  (repl))
