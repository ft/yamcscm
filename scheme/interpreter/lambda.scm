;; -*- scheme -*-

(define-module (interpreter lambda)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (interpreter closure)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda sugar)
  #:export (λ-eval λ-apply))

(define* (λ-eval expr #:optional (env (make-empty-environment)))
  (match (desugar expr)
    ((? closure? e) e)
    ((? symbol? e) (lookup env e))                     ; Variables
    (('λ ((? symbol? a)) e) (make-closure a e env))    ; Abstraction
    ((operator operand) (λ-apply (λ-eval operator env) ; Application
                                 (λ-eval operand env)))))

(define (λ-apply operator operand)
  (λ-eval (closure-body operator)
          (extend* (closure-environment operator)
                   (closure-formals operator)
                   operand)))
