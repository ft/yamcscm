;; -*- scheme -*-

(define-module (interpreter lambda natural-numbers)
  #:use-module (interpreter environment)
  #:use-module (interpreter lambda)
  #:use-module (interpreter lambda boolean)
  #:use-module (interpreter lambda combinators)
  #:use-module (interpreter lambda pair)
  #:export (natural-numbers))

(define λ-zero      (lookup combinators 'KI))
(define λ-successor (λ-eval '(λn → λf → λz → f (n f z))))

(define construction
  (make-environment `((zero      . ,λ-zero)
                      (successor . ,λ-successor))))

(define λ-plus  (λ-eval '(λa → λb → a successor b) construction))
(define λ-times (lookup combinators 'B))
(define λ-power (lookup combinators 'Th))
(define λ-zero? (λ-eval '(λn → n (true zero) true) (use boolean construction)))
(define λ-predecessor (λ-eval '(λn → first (n (λp → cons
                                                    (second p)
                                                    (successor (second p)))
                                              (cons zero zero)))
                              (use construction pair)))

(define basic
  (use (make-environment `((+           . ,λ-plus)
                           (*           . ,λ-times)
                           (^           . ,λ-power)
                           (zero?       . ,λ-zero?)
                           (predecessor . ,λ-predecessor)))
       construction))

(define λ-minus (λ-eval '(λn → λk → k predecessor n) basic))

(define advanced
  (use (make-environment `((- . ,λ-minus)))
       basic))

(define λ-less-or-equal (λ-eval '(λn → λk → zero? (- n k)) advanced))

(define more-advanced
  (use (make-environment `((<= . ,λ-less-or-equal)))
       advanced))

(define λ-equal (λ-eval '(λn → λk → and (<= n k) (<= k n)) (use boolean
                                                                more-advanced)))

(define natural-numbers
  (use (make-environment `((= . ,λ-equal)))
       more-advanced))
