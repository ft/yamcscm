;; -*- scheme -*-

(define-module (interpreter primitive)
  #:use-module (srfi srfi-9 gnu)
  #:export (primitive?
            primitive-operator?
            primitive-operator
            primitive-name
            primitive-procedure))

(define-immutable-record-type <primitive>
  (make-primitive name procedure)
  primitive?
  (name primitive-name)
  (procedure primitive-procedure))

(define-syntax-rule (define-primitives var p ...)
  (define var (list (make-primitive 'p p) ...)))

(define-primitives primitives
  1+ + - * / < > = zero? positive? negative?
  eq? eqv? equal?
  list cons car cdr)

(define (primitive-operator op)
  (let loop ((rest primitives))
    (cond ((null? rest) #f)
          ((eq? (primitive-name (car rest)) op) (car rest))
          (else (loop (cdr rest))))))

(define (primitive-operator? op)
  (not (not (primitive-operator op))))
