;; -*- scheme -*-

(define-module (interpreter eval-apply)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (interpreter closure)
  #:use-module (interpreter environment)
  #:use-module (interpreter primitive)
  #:export (xapply xeval))

(define* (xeval expr #:optional (env (make-empty-environment)))
  (match expr
    ((? self-evaluating? literal) literal)   ; Literal Values
    ((? primitive-operator? e)               ; Primitives
     (primitive-operator e))
    ((? symbol? e) (lookup env e))           ; Variables
    (('lambda ((? symbol? a) ...) b ..1)     ; Abstraction
     (make-closure a b env))
    (('lambda (? symbol? a) b ..1)
     (make-closure a b env))
    (('if condition consequence alternative) ; Conditional Evaluation
     (if (xeval condition env)
         (xeval consequence env)
         (xeval alternative env)))
    (('apply operator operands)              ; Expose apply Operator
     (xeval (cons (xeval operator env)
                  (xeval operands env))
            env))
    ((operator . operands)                   ; Application
     (xapply (xeval operator env)
             (list-eval operands env)))))

(define (xapply operator operands)
  (cond ((primitive? operator)
         (apply (primitive-procedure operator) operands))
        ((closure? operator)
         (apply-closure operator operands))
        (else (error "xapply: Expected closure or primitive:" operator))))

(define* (apply-closure closure operands)
  (let ((formals (closure-formals closure))
        (env (closure-environment closure)))
    (unless (or (symbol? formals)
                (= (length formals)
                   (length operands)))
      (error "Invalid number of arguments: Expected:" (length formals)
             'got: (length operands) closure operands))
    (sequence-eval (closure-body closure)
                   (extend* env formals operands))))

(define (list-eval lst env)
  (map (lambda (e) (xeval e env)) lst))

(define (sequence-eval body env)
  (let ((value (xeval (car body) env))
        (rest (cdr body)))
    (if (null? rest)
        value
        (sequence-eval rest env))))
